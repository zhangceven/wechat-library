package com.techblor.library.wechat.constants;

/**
 * 语言常量
 */
public interface LangConstant {

    /**
     * 简体中文
     */
    String ZH_CN = "zh_CN";

    /**
     * 英语
     */
    String EN_US = "en_US";

    /**
     * 繁体中文: 香港
     */
    String ZH_HK = "zh_HK";

    /**
     * 繁体中文: 台湾
     */
    String ZH_TW = "zh_TW";
}
