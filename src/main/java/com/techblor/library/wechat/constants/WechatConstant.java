package com.techblor.library.wechat.constants;

/**
 * api常量
 *
 * @author zhangceven
 */
public interface WechatConstant {

    /**
     * 微信API请求地址域名
     */
    String BASE_URL = "https://api.weixin.qq.com";

    /**
     * 微信文件请求地址域名
     */
    String MEDIA_URI = "http://file.api.weixin.qq.com";

    /**
     * 微信开放平台请求地址域名
     */
    String OPEN_URI = "https://open.weixin.qq.com";

}
