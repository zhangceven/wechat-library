package com.techblor.library.wechat.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * 字符串处理工具类
 *
 * @author zhangceven
 */
@Slf4j
public class StringUtil {

    /**
     * 判断传入字符串是否为空(不能判断null值)
     *
     * @param value 传入的字符串
     * @return
     */
    public static boolean isEmpty(CharSequence value) {
        return value == null || value.length() == 0;
    }

    /**
     * 判断传入字符串是否不为空(不能判断null值)
     *
     * @param value 传入的字符串
     * @return
     */
    public static boolean isNotEmpty(CharSequence value) {
        return value != null && value.length() > 0;
    }

    /**
     * 是否为空字符串(引号中间有空格)  如： " "。
     * 适用于制表符、换行符、换页符和回车
     * @param value
     * @return
     */
    public static boolean isBlank(CharSequence value) {
		int strLen;
		if (value == null || (strLen = value.length()) == 0) {
			return true;
		}

		for (int i = 0; i < strLen; i++) {
			if (!Character.isWhitespace(value.charAt(i))) {
				return false;
			}
		}

		return true;
	}
    
    /**
     * 是否不为空字符串(引号中间有空格)  如： " "。
     * @param value
     * @return
     */
    public static boolean isNotBlank(CharSequence value) {
		return !isBlank(value);
	}

    /**
     * 判断多个参数中是否包含空字符串
     * @param value
     * @return
     */
	public static boolean hasBlank(CharSequence ... value){
        if(value != null){
            for (CharSequence character : value) {
                if(null == character || character.length() <= 0 || "null".equals(character)){
                    return true;
                }
            }
        }else{
            return false;
        }

        return false;
    }

    /**
     * 返回将字符串集合转换成以逗号隔开的字符串数据
     *
     * @param list
     * @param separator
     * @return
     */
    public static String join(List<String> list, String separator) {
        if(list == null || list.isEmpty()){
            return null;
        }

        Iterator<?> iterator = list.iterator();
        if (!iterator.hasNext()) {
            return null;
        }

        Object first = iterator.next();
        if (!iterator.hasNext()) {
            return first.toString();
        } else {
            StringBuilder sb = new StringBuilder(256);
            if (first != null) {
                sb.append(first);
            }

            while (iterator.hasNext()) {
                sb.append(separator);
                Object next = iterator.next();
                if (next != null) {
                    sb.append(next);
                }
            }

            return sb.toString();
        }
    }

    /**
     * 生成UUID
     * @return
     */
    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }

}
