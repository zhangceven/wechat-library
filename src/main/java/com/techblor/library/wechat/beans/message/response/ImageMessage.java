package com.techblor.library.wechat.beans.message.response;

/**
 * 回复图片消息
 * @author zhangceven
 *
 */
public class ImageMessage extends BaseMessage {

	private Image Image; // 图片

	public Image getImage() {
		return Image;
	}

	public void setImage(Image image) {
		Image = image;
	}
	
}
