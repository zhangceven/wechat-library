package com.techblor.library.wechat.beans.miniprogram.result.subscribemessage;

import com.techblor.library.wechat.beans.miniprogram.beans.SubscribeMessageCategory;
import com.techblor.library.wechat.beans.result.BaseResult;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 订阅消息获取小程序类目结果对象
 */
@Getter
@Setter
public class GetCategoryResult extends BaseResult {

    /**
     * 类目列表
     */
    private List<SubscribeMessageCategory> data;
}
