package com.techblor.library.wechat.beans.miniprogram.params;

import com.alibaba.fastjson.annotation.JSONField;
import com.techblor.library.wechat.beans.miniprogram.beans.LineColor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 小程序码参数对象
 */
@Getter
@Setter
@Builder
public class WxaCodeParams {

    /**
     * 扫码进入的小程序页面路径,
     * 最大长度 128 字节，不能为空;
     * 对于小游戏，可以只传入 query 部分,
     * 来实现传参效果,
     * 如：传入 "?foo=bar",
     * 即可在 wx.getLaunchOptionsSync 接口中的 query 参数获取到 {foo:"bar"}。
     */
    private String path;

    /**
     * 二维码的宽度，单位 px。最小 280px，最大 1280px
     */
    private Integer width;

    /**
     * 自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调
     */
    @JSONField(name = "auto_color")
    private Boolean autoColor;

    /**
     * auto_color 为 false 时生效,
     * 使用 rgb 设置颜色
     * 例如 {"r":"xxx","g":"xxx","b":"xxx"} 十进制表示
     */
    @JSONField(name = "line_color")
    private LineColor lineColor;

    /**
     * 是否需要透明底色，为 true 时，生成透明底色的小程序码
     */
    @JSONField(name = "is_hyaline")
    private Boolean isHyaline;
}
