package com.techblor.library.wechat.beans.live;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 直播间信息
 */
@Getter
@Setter
public class RoomInfo {

    /**
     * 直播间名称
     */
    private String name;

    /**
     * 直播间ID
     */
    private Integer roomid;

    /**
     * 直播间背景图链接
     */
    private String cover_img;

    /**
     * 直播间分享图链接
     */
    private String share_img;

    /**
     * 直播间状态。101：直播中，102：未开始，103已结束，104禁播，105：暂停，106：异常，107：已过期
     */
    private Integer live_status;

    /**
     * 直播间开始时间，列表按照start_time降序排列
     */
    private Long start_time;

    /**
     * 直播计划结束时间
     */
    private Long end_time;

    /**
     * 主播名
     */
    private String anchor_name;

    /**
     * 直播间商品列表
     */
    private List<Goods> goods;
}
