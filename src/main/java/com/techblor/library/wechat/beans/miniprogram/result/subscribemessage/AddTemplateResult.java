package com.techblor.library.wechat.beans.miniprogram.result.subscribemessage;

import com.techblor.library.wechat.beans.result.BaseResult;
import lombok.Getter;
import lombok.Setter;

/**
 * 订阅消息添加模板结果对象
 */
@Getter
@Setter
public class AddTemplateResult extends BaseResult {

    /**
     * 添加至帐号下的模板id，发送小程序订阅消息时所需
     */
    String priTmplId;
}
