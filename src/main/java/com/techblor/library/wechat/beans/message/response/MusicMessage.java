package com.techblor.library.wechat.beans.message.response;

/**
 * 回复音乐消息
 * @author zhangceven
 *
 */
public class MusicMessage extends BaseMessage{
	
	private Music Music; // 音乐

	public Music getMusic() {
		return Music;
	}

	public void setMusic(Music music) {
		Music = music;
	}
	
	

}
