package com.techblor.library.wechat.beans.menu;

import lombok.Getter;
import lombok.Setter;

/**
 * 当前定义菜单
 */
@Getter
@Setter
public class CurrentSelfMenuInfo {

    /**
     * 菜单是否开启，0代表未开启，1代表开启
     */
    private Integer is_menu_open;

    /**
     * 菜单信息
     */
    private SelfMenuInfo selfmenu_info;
}
