package com.techblor.library.wechat.beans.miniprogram.beans;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhoneNumberInfo {

    /**
     * 用户绑定的手机号（国外手机号会有区号）
     */
    private String phoneNumber;

    /**
     * 没有区号的手机号
     */
    private String purePhoneNumber;

    /**
     * 区号
     */
    private String countryCode;

    /**
     * 水印
     */
    private Watermark watermark;
}
