package com.techblor.library.wechat.beans.live;

import com.techblor.library.wechat.beans.result.BaseResult;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class LiveInfo extends BaseResult {

    private Integer total;

    private List<RoomInfo> room_info;
}
