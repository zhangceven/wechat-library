package com.techblor.library.wechat.beans.message.response;

/**
 * 回复语音消息
 * @author zhangceven
 *
 */
public class VoiceMessage extends BaseMessage {

	private Voice Voice; // 语音

	public Voice getVoice() {
		return Voice;
	}

	public void setVoice(Voice voice) {
		Voice = voice;
	}
	
	
}
