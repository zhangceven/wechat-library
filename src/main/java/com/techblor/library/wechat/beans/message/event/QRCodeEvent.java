package com.techblor.library.wechat.beans.message.event;

/**
 * 扫描带参二维码事件
 * @author zhangceven
 *
 */
public class QRCodeEvent extends BaseEvent{

	private String EventKey; // 事件KEY值
	
	private String Ticket; // 用于换取二维码图片

	public String getEventKey() {
		return EventKey;
	}

	public void setEventKey(String eventKey) {
		EventKey = eventKey;
	}

	public String getTicket() {
		return Ticket;
	}

	public void setTicket(String ticket) {
		Ticket = ticket;
	}
	
}
