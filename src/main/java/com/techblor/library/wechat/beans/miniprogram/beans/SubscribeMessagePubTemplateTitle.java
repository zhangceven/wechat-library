package com.techblor.library.wechat.beans.miniprogram.beans;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubscribeMessagePubTemplateTitle {
    /**
     * 模版标题 id
     */
    private Integer tid;

    /**
     * 模版标题
     */
    private String title;

    /**
     * 模版类型: 2 为一次性订阅，3 为长期订阅
     */
    private Integer type;

    /**
     * 模版所属类目 id
     */
    private Integer categoryId;
}
