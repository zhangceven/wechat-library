package com.techblor.library.wechat.beans.result;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;

/**
 * 微信返回结果对象
 *
 * @author zhangceven
 */
@Getter
@Setter
public class BaseResult {

    /**
     * 微信返回结果码
     */
    private Integer errcode;

    /**
     * 微信返回结果信息描述
     */
    private String errmsg;

}
