package com.techblor.library.wechat.beans.message.request;

/**
 * 接收图片消息
 * @author zhangceven
 *
 */
public class ImageMessage extends BaseMessage {
	
	private String PicUrl;	// 图片链接
	
	private String MediaId;	// 上传图片返回的id
	
	private Long MsgId; // 消息id,64位整型

	public String getPicUrl() {
		return PicUrl;
	}

	public void setPicUrl(String picUrl) {
		PicUrl = picUrl;
	}

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	public Long getMsgId() {
		return MsgId;
	}

	public void setMsgId(Long msgId) {
		MsgId = msgId;
	}
	
}
