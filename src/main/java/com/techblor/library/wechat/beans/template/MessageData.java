package com.techblor.library.wechat.beans.template;

import lombok.*;

/**
 * 模板消息数据
 *
 * @author zhangceven
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageData {

    /**
     * 消息
     */
    private String value;

    /**
     * 颜色
     */
    private String color;

}
