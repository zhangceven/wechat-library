package com.techblor.library.wechat.beans.miniprogram.beans;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 线条颜色
 */
@Getter
@Setter
@Builder
public class LineColor {

    /**
     * R十进制色值
     */
    private Integer r;

    /**
     * G十进制色值
     */
    private Integer g;

    /**
     * B十进制色值
     */
    private Integer b;
}
