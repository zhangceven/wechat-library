package com.techblor.library.wechat.beans.message.response;

/**
 * 语音消息
 * @author zhangceven
 *
 */
public class Voice {

	private String MediaId; // 媒体ID

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}
	
	
}
