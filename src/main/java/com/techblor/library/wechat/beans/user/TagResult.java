package com.techblor.library.wechat.beans.user;

import com.techblor.library.wechat.beans.result.BaseResult;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * 获取公众号已创建的标签
 *
 * @author zhangceven
 */
@Getter
@Setter
public class TagResult extends BaseResult {

    private List<Tag> tags;

}
