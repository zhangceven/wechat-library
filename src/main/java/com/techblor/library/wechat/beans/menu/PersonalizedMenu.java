package com.techblor.library.wechat.beans.menu;

import lombok.Getter;
import lombok.Setter;

/**
 * 个性化菜单
 */
@Getter
@Setter
public class PersonalizedMenu extends CustomMenu {

    /**
     * 说明: 菜单匹配规则
     * 是否必须: 是
     */
    private Matchrule matchrule;
}
