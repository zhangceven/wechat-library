package com.techblor.library.wechat.beans.live;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 直播间商品
 */
@Getter
@Setter
public class Goods {

    /**
     * 商品封面图链接
     */
    private String cover_img;

    /**
     * 商品小程序路径
     */
    private String url;

    /**
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 商品名称
     */
    private String name;
}
