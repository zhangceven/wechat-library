package com.techblor.library.wechat.beans.user;

import com.techblor.library.wechat.beans.result.BaseResult;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * SnsUserInfo
 * 通过网页授权获取的用户信息
 *
 * @author zhangceven
 */
@Getter
@Setter
public class SnsapiUserInfo extends BaseResult {

    /**
     * 公众号用户openid
     */
    private String openid;

    /**
     * 公众平台用户unionid
     */
    private String unionid;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 性别（1是男性，2是女性，0是未知）
     */
    private Integer sex;

    /**
     * 国家
     */
    private String country;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 用户头像链接
     */
    private String headimgurl;

    /**
     * 用户特权信息
     */
    private List<String> privilege;

}