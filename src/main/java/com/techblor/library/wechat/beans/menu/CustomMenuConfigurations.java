package com.techblor.library.wechat.beans.menu;

import lombok.Getter;
import lombok.Setter;

/**
 * 个性化菜单配置
 */
@Getter
@Setter
public class CustomMenuConfigurations {

    /**
     * 自定义菜单
     */
    private CustomMenu menu;
}
