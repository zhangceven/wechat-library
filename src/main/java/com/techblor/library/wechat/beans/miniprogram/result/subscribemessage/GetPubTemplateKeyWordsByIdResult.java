package com.techblor.library.wechat.beans.miniprogram.result.subscribemessage;

import com.techblor.library.wechat.beans.miniprogram.beans.SubscribeMessagePubTemplateKeyWords;
import com.techblor.library.wechat.beans.result.BaseResult;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 获取模板标题下的关键词列表结果对象
 */
@Getter
@Setter
public class GetPubTemplateKeyWordsByIdResult extends BaseResult {

    /**
     * 模版标题列表总数
     */
    private Integer count;

    /**
     * 关键词列表
     */
    private List<SubscribeMessagePubTemplateKeyWords> data;
}
