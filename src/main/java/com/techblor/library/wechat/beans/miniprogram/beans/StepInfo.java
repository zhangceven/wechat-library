package com.techblor.library.wechat.beans.miniprogram.beans;

import lombok.Getter;
import lombok.Setter;

/**
 * 微信运动信息
 */
@Getter
@Setter
public class StepInfo {

    /**
     * 时间戳, 表示数据对应的时间: 单位/秒
     */
    private Long timestamp;

    /**
     * 微信运动步数
     */
    private Integer step;
}
