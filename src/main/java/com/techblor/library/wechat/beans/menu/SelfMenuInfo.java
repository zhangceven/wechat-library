package com.techblor.library.wechat.beans.menu;

import lombok.Getter;
import lombok.Setter;

/**
 * 菜单信息
 */
@Getter
@Setter
public class SelfMenuInfo extends CustomMenu {
}
