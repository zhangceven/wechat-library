package com.techblor.library.wechat.beans.template;

import java.util.HashMap;

/**
 * 模板消息数据
 */
public class TemplateMessageData extends HashMap<String, MessageData> {

    private TemplateMessageData() {
    }

    /**
     * 获取TemplateMessageData实例
     *
     * @return
     */
    public static TemplateMessageData getInstance() {
        return new TemplateMessageData();
    }

    /**
     * 添加模板消息数据
     *
     * @param key
     * @param value
     * @param color
     * @return
     */
    public TemplateMessageData add(String key, String value, String color) {
        this.put(key, new MessageData(value, color));
        return this;
    }
}
