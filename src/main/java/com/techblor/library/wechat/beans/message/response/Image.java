package com.techblor.library.wechat.beans.message.response;

/**
 * 图片
 * @author zhangceven
 *
 */
public class Image {
	
	private String MediaId; // 媒体ID

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}
	
	

}
