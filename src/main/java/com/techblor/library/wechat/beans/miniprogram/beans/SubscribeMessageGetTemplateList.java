package com.techblor.library.wechat.beans.miniprogram.beans;

import lombok.Getter;
import lombok.Setter;

/**
 * 个人模板
 */
@Getter
@Setter
public class SubscribeMessageGetTemplateList {

    /**
     * 添加至帐号下的模板 id: 发送小程序订阅消息时所需
     */
    private String priTmplId;

    /**
     * 模版标题
     */
    private String title;

    /**
     * 模版内容
     */
    private String content;

    /**
     * 模板内容示例
     */
    private String example;

    /**
     * 模版类型: 2 为一次性订阅, 3 为长期订阅
     */
    private Integer type;

}
