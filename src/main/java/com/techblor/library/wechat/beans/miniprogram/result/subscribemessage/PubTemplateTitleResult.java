package com.techblor.library.wechat.beans.miniprogram.result.subscribemessage;

import com.techblor.library.wechat.beans.miniprogram.beans.SubscribeMessagePubTemplateTitle;
import com.techblor.library.wechat.beans.result.BaseResult;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 模版标题列表
 */
@Getter
@Setter
public class PubTemplateTitleResult extends BaseResult {

    /**
     * 模版标题列表总数
     */
    private Integer count;

    /**
     * 模版标题列表
     */
    private List<SubscribeMessagePubTemplateTitle> data;
}
