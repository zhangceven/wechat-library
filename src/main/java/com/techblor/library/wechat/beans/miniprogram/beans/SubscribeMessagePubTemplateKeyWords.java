package com.techblor.library.wechat.beans.miniprogram.beans;

import lombok.Getter;
import lombok.Setter;

/**
 * 模板标题关键词
 */
@Getter
@Setter
public class SubscribeMessagePubTemplateKeyWords {

    /**
     * 关键词 id; 选用模板时需要
     */
    private Integer kid;

    /**
     * 关键词内容
     */
    private String name;

    /**
     * 关键词内容对应的示例
     */
    private String example;

    /**
     * 参数类型
     */
    private String rule;
}
