package com.techblor.library.wechat.beans.officialaccount.params;

import lombok.Getter;
import lombok.Setter;

/**
 * openid参数
 */
@Getter
@Setter
public class UserInfoParam {

    /**
     * 用户的标识，对当前公众号唯一
     */
    private String openid;

    /**
     * 国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语，默认为zh-CN
     */
    private String lang;
}
