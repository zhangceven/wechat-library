package com.techblor.library.wechat.beans.menu;

import com.techblor.library.wechat.beans.result.BaseResult;
import lombok.Getter;
import lombok.Setter;

/**
 * 个性化菜单创建结果
 */
@Getter
@Setter
public class PersonalizedMenuCreateResult extends BaseResult {

    /**
     * 菜单ID
     */
    private String menuid;
}
