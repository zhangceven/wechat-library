package com.techblor.library.wechat.beans.miniprogram.params.subscribemessage;

import lombok.*;

/**
 * 订阅消息模板数据值
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TemplateParamsDataValue {

    /**
     * 参数值
     */
    private String value;
}