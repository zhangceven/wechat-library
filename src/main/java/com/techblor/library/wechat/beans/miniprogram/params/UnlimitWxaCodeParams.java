package com.techblor.library.wechat.beans.miniprogram.params;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 无限制小程序码参数对象
 */
@Setter
@Getter
@Builder
public class UnlimitWxaCodeParams {

    /**
     * 最大32个可见字符, 只支持数字, 大小写英文以及部分特殊字符：
     * !#$&'()*+,/:;=?@-._~
     * 其它字符请自行编码为合法字符(因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式)
     */
    private String scene;

    /**
     * 扫码进入的小程序页面路径,
     * 最大长度 128 字节，不能为空;
     * 对于小游戏，可以只传入 query 部分,
     * 来实现传参效果,
     * 如：传入 "?foo=bar",
     * 即可在 wx.getLaunchOptionsSync 接口中的 query 参数获取到 {foo:"bar"}。
     */
    private String page;

    /**
     * 二维码的宽度，单位 px。最小 280px，最大 1280px
     */
    private Integer width;

    /**
     * 自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调
     */
    @JSONField(name = "auto_color")
    private Boolean autoColor;

    /**
     * auto_color 为 false 时生效,
     * 使用 rgb 设置颜色
     * 例如 {"r":"xxx","g":"xxx","b":"xxx"} 十进制表示
     */
    @JSONField(name = "line_color")
    private String lineColor;

    /**
     * 是否需要透明底色，为 true 时，生成透明底色的小程序码
     */
    @JSONField(name = "is_hyaline")
    private Boolean isHyaline;

}
