package com.techblor.library.wechat.beans.miniprogram.beans;

import lombok.Getter;
import lombok.Setter;

import java.awt.image.BufferedImage;

/**
 * 小程序码
 */
@Getter
@Setter
public class WxaCode {

    /**
     * 数据类型 (MIME Type)
     */
    private String contentType;

    /**
     * 小程序码 Buffer
     */
    private BufferedImage buffer;
}
