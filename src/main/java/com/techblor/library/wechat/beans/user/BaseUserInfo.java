package com.techblor.library.wechat.beans.user;

import com.techblor.library.wechat.beans.result.BaseResult;
import lombok.Getter;
import lombok.Setter;
import org.json.JSONArray;

import java.util.List;

/**
 * BaseUserInfo
 * 微信用户的基本信息
 *
 * @author zhangceven
 */
@Getter
@Setter
public class BaseUserInfo extends BaseResult {

    /**
     * 用户的标识
     */
    private String openid;

    /**
     * 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
     */
    private String unionid;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 用户的性别（1是男性，2是女性，0是未知）
     */
    private Integer sex;

    /**
     * 用户所在国家
     */
    private String country;

    /**
     * 用户所在省份
     */
    private String province;

    /**
     * 用户所在城市
     */
    private String city;

    /**
     * 用户的语言，简体中文为zh_CN
     */
    private String language;

    /**
     * 用户头像
     */
    private String headimgurl;

    /**
     * 关注状态（1是关注，0是未关注），未关注时获取不到其余信息
     */
    private Integer subscribe;

    /**
     * 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
     */
    private Long subscribe_time;

    /**
     * 公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注
     */
    private String remark;

    /**
     * 用户所在的分组ID（兼容旧的用户分组接口）
     */
    private Integer groupid;

    /**
     * 用户被打上的标签ID列表
     */
    private List<Integer> tagid_list;

    /**
     * 返回用户关注的渠道来源
     * ADD_SCENE_SEARCH 公众号搜索
     * ADD_SCENE_ACCOUNT_MIGRATION 公众号迁移
     * ADD_SCENE_PROFILE_CARD 名片分享
     * ADD_SCENE_QR_CODE 扫描二维码
     * ADD_SCENE_PROFILE_LINK 图文页内名称点击
     * ADD_SCENE_PROFILE_ITEM 图文页右上角菜单
     * ADD_SCENE_PAID 支付后关注
     * ADD_SCENE_WECHAT_ADVERTISEMENT 微信广告
     * ADD_SCENE_OTHERS 其他
     */
    private String subscribe_scene;

    /**
     * 二维码扫码场景（开发者自定义）
     */
    private Integer qr_scene;

    /**
     * 二维码扫码场景描述（开发者自定义）
     */
    private String qr_scene_str;
}