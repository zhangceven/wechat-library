package com.techblor.library.wechat.beans.message.response;

/**
 * 视频消息
 * @author Administrator
 *
 */
public class Video {

	private String MediaId; // 媒体ID
	
	private String ThumbMediaId; // 缩略图媒体ID

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	public String getThumbMediaId() {
		return ThumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		ThumbMediaId = thumbMediaId;
	}
	
}
