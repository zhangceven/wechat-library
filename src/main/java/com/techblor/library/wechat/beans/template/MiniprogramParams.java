package com.techblor.library.wechat.beans.template;

import lombok.*;

/**
 * 跳小程序所需数据
 * 不需跳小程序可不用传该数据
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MiniprogramParams {

    /**
     * 小程序appid
     */
    private String appid;

    /**
     * 跳转页面
     */
    private String pagepath;
}
