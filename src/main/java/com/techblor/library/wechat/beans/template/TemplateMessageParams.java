package com.techblor.library.wechat.beans.template;

import lombok.*;

/**
 * 模板消息
 *
 * @author zhangceven
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TemplateMessageParams {

    /**
     * 用户的OpenID
     */
    private String touser;

    /**
     * 模板ID
     */
    private String template_id;

    /**
     * 点击消息请求的url
     */
    private String url;

    /**
     * 跳小程序所需数据
     * 不需跳小程序可不用传该数据
     */
    private MiniprogramParams miniprogram;

    /**
     * 模板消息数据
     */
    private TemplateMessageData data;

}
