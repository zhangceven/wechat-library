package com.techblor.library.wechat.beans.token;


import com.techblor.library.wechat.beans.result.BaseResult;
import lombok.Getter;
import lombok.Setter;

/**
 * token 封装access_token
 *
 * @author zhangceven
 */
@Getter
@Setter
public class AccessToken extends BaseResult {

    /**
     * 接口访问凭证
     */
    private String access_token;

    /**
     * 凭证有效时间(单位/秒)
     */
    private int expires_in;

}
