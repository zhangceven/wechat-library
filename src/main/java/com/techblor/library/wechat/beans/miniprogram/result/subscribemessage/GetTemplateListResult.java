package com.techblor.library.wechat.beans.miniprogram.result.subscribemessage;

import com.techblor.library.wechat.beans.miniprogram.beans.SubscribeMessageGetTemplateList;
import com.techblor.library.wechat.beans.result.BaseResult;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 获取当前帐号下的个人模板列表
 */
@Getter
@Setter
public class GetTemplateListResult extends BaseResult {

    /**
     * 个人模板列表
     */
    private List<SubscribeMessageGetTemplateList> data;
}
