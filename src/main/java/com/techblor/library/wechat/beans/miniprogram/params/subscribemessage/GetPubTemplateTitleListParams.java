package com.techblor.library.wechat.beans.miniprogram.params.subscribemessage;

import lombok.*;

/**
 * 获取帐号所属类目下的公共模板标题参数
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetPubTemplateTitleListParams {

    /**
     * 类目 id: 多个用逗号隔开
     */
    private String ids;

    /**
     * 用于分页: 表示从 start 开始。从 0 开始计数。
     */
    private Integer start;

    /**
     * 用于分页，表示拉取 limit 条记录。最大为 30。
     */
    private Integer limit;
}
