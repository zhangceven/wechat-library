package com.techblor.library.wechat.beans.miniprogram.beans;

import lombok.Getter;
import lombok.Setter;

/**
 * 水印
 *
 * @author zhangceven
 */
@Getter
@Setter
public class Watermark {

    private String appid;

    private String timestamp;

}
