package com.techblor.library.wechat.beans.miniprogram.beans;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RunData {

    private List<StepInfo> stepInfoList;
}
