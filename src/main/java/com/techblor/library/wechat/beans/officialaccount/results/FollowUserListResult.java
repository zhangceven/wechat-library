package com.techblor.library.wechat.beans.officialaccount.results;

import com.techblor.library.wechat.beans.result.BaseResult;
import com.techblor.library.wechat.beans.user.OpenidList;
import lombok.Getter;
import lombok.Setter;

/**
 * 粉丝用户列表
 */
@Getter
@Setter
public class FollowUserListResult extends BaseResult {

    /**
     * 关注该公众账号的总用户数
     */
    private Integer total;

    /**
     * 拉取的OPENID个数，最大值为10000
     */
    private Integer count;

    /**
     * 列表数据，OPENID的列表
     */
    private OpenidList data;

    /**
     * 拉取列表的最后一个用户的OPENID
     */
    private String next_openid;
}
