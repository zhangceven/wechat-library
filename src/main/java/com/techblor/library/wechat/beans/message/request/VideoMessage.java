package com.techblor.library.wechat.beans.message.request;

/**
 * 接收视频消息
 * @author zhangceven
 *
 */
public class VideoMessage extends BaseMessage{

	private String MediaId;	// 媒体ID
	
	private String ThumbMediaId; // 语音格式

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	public String getThumbMediaId() {
		return ThumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		ThumbMediaId = thumbMediaId;
	}
	
}
