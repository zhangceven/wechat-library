package com.techblor.library.wechat.beans.miniprogram.beans;

import lombok.Getter;
import lombok.Setter;

/**
 * 小程序用户
 */
@Getter
@Setter
public class MiniProgramUserInfo {

    private String openid;

    private String unionId;

    private String nickName;

    private String avatarUrl;

    private String gender;

    private String city;

    private String province;

    private String country;

    private String language;

    private Watermark watermark;
}
