package com.techblor.library.wechat.beans.message.response;

/**
 * 回复文本信息
 * @author zhangceven
 *
 */
public class TextMessage extends BaseMessage {

	private String Content; // 回复的消息内容

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}
	
}
