package com.techblor.library.wechat.beans.user;


import lombok.Getter;
import lombok.Setter;

/**
 * 标签
 *
 * @author zhangceven
 */
@Getter
@Setter
public class Tag {

    /**
     * 标签id，由微信分配
     */
    private Integer id;

    /**
     * 标签名，UTF8编码
     */
    private String name;

    /**
     * 此标签下粉丝数
     */
    private Long count;

}
