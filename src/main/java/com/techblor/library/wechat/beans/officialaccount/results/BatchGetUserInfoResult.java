package com.techblor.library.wechat.beans.officialaccount.results;

import com.techblor.library.wechat.beans.result.BaseResult;
import com.techblor.library.wechat.beans.user.BaseUserInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 批量获取用户信息结果对象
 */
@Getter
@Setter
public class BatchGetUserInfoResult extends BaseResult {

    /**
     * 用户信息列表
     */
    private List<BaseUserInfo> user_info_list;
}
