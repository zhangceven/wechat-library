package com.techblor.library.wechat.beans.user;

import lombok.Getter;
import lombok.Setter;

/**
 * 标签下的粉丝列表
 *
 * @author zhangceven
 */
@Getter
@Setter
public class UserTags {

    private int count;

    private OpenidList data;

    private String next_openid;

}


