package com.techblor.library.wechat.beans.officialaccount.params;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 批量获取用户信息参数
 */
@Getter
@Setter
public class BatchGetUserInfoParam {

    /**
     * 获取用户信息列表参数
     */
    private List<UserInfoParam> user_list;
}
