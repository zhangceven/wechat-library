package com.techblor.library.wechat.beans.miniprogram.beans;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubscribeMessageCategory {

    /**
     * 类目id，查询公共库模版时需要
     */
    private String id;

    /**
     * 类目的中文名
     */
    private String name;
}
