package com.techblor.library.wechat.beans.user;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * openid集合
 *
 * @author zhangceven
 */
@Getter
@Setter
public class OpenidList {

    /**
     * 用户openid列表
     */
    private List<String> openid;

}