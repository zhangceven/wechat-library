package com.techblor.library.wechat.beans.message.request;

import lombok.Data;

/**
 * 接收消息基类
 * @author zhangceven
 *
 */
@Data
public class BaseMessage {

	/**
	 * 开发者微信号
	 */
	private String ToUserName;		// 开发者微信号

	/**
	 * 发送方账号(一个openid)
	 */
	private String FromUserName;	// 发送方账号(一个OpenID)

	/**
	 * 消息创建时间(整型)
	 */
	private Long CreateTime;		// 消息创建时间(整型)

	/**
	 * 消息类型
	 */
	private String MsgType;			// text


}
