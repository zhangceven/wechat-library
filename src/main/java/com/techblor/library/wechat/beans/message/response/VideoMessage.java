package com.techblor.library.wechat.beans.message.response;

/**
 * 回复视频消息 
 * @author zhangceven
 *
 */
public class VideoMessage extends BaseMessage {

	private Video Video; // 视频

	public Video getVideo() {
		return Video;
	}

	public void setVideo(Video video) {
		Video = video;
	}
	
}
