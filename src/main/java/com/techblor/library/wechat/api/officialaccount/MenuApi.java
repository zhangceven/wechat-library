package com.techblor.library.wechat.api.officialaccount;

import com.alibaba.fastjson.JSON;
import com.techblor.library.wechat.beans.menu.*;
import com.techblor.library.wechat.beans.result.BaseResult;
import com.techblor.library.wechat.utils.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * 自定义菜单API
 *
 * @author zhangceven
 */
@Slf4j
public class MenuApi {

    /**
     * 通过菜单json数据创建微信公众号菜单
     *
     * @param access_token   接口调用令牌
     * @param customMenuJson 自定义菜单json
     * @return
     */
    public static BaseResult createMenu(String access_token, String customMenuJson) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" + access_token;
        String result = HttpClientUtil.doPostJson(url, customMenuJson);
        return JSON.parseObject(result, BaseResult.class);
    }

    /**
     * 通过传入菜单对象创建微信公众号菜单
     *
     * @param access_token 接口调用令牌
     * @param customMenu   自定义菜单
     * @return
     */
    public static BaseResult createMenu(String access_token, CustomMenu customMenu) {
        String menuJson = JSON.toJSONString(customMenu);
        return createMenu(access_token, menuJson);

    }

    /**
     * 获取菜单
     *
     * @param access_token
     * @return
     */
    public static CustomMenuConfigurations getMenu(String access_token) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=" + access_token;
        String result = HttpClientUtil.doGet(url);
        return JSON.parseObject(result, CustomMenuConfigurations.class);
    }

    /**
     * 删除菜单
     *
     * @param access_token
     * @return
     */
    public static BaseResult deleteMenu(String access_token) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=" + access_token;
        String result = HttpClientUtil.doGet(url);
        return JSON.parseObject(result, BaseResult.class);
    }

    /**
     * 本接口将会提供公众号当前使用的自定义菜单的配置，如果公众号是通过API调用设置的菜单，则返回菜单的开发配置，而如果公众号是在公众平台官网通过网站功能发布菜单，则本接口返回运营者设置的菜单配置。
     * <p>
     * 请注意：
     * <p>
     * 第三方平台开发者可以通过本接口，在旗下公众号将业务授权给你后，立即通过本接口检测公众号的自定义菜单配置，并通过接口再次给公众号设置好自动回复规则，以提升公众号运营者的业务体验。
     * 本接口与自定义菜单查询接口的不同之处在于，本接口无论公众号的接口是如何设置的，都能查询到接口，而自定义菜单查询接口则仅能查询到使用API设置的菜单配置。
     * 认证/未认证的服务号/订阅号，以及接口测试号，均拥有该接口权限。
     * 从第三方平台的公众号登录授权机制上来说，该接口从属于消息与菜单权限集。
     * 本接口中返回的图片/语音/视频为临时素材（临时素材每次获取都不同，3天内有效，通过素材管理-获取临时素材接口来获取这些素材），本接口返回的图文消息为永久素材素材（通过素材管理-获取永久素材接口来获取这些素材）。
     * <p>
     * 如果公众号是在公众平台官网通过网站功能发布菜单，则本接口返回的自定义菜单配置样例如下
     *
     * @param access_token
     * @return
     */
    public static CurrentSelfMenuInfo getCurrentSelfMenuInfo(String access_token) {
        String url = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=" + access_token;
        String result = HttpClientUtil.doGet(url);
        return JSON.parseObject(result, CurrentSelfMenuInfo.class);
    }

    /**
     * 创建个性化菜单
     *
     * @param access_token     接口调用令牌
     * @param personalizedMenu 个性化菜单
     * @return
     */
    public static BaseResult createPersonalizedMenu(String access_token, PersonalizedMenu personalizedMenu) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=" + access_token;
        String personalizedMenuJson = JSON.toJSONString(personalizedMenu);
        String result = HttpClientUtil.doPostJson(url, personalizedMenuJson);
        return JSON.parseObject(result, PersonalizedMenuCreateResult.class);
    }

    /**
     * 删除个性化菜单
     *
     * @param access_token 接口调用令牌
     * @param menuid       菜单id
     * @return
     */
    public static BaseResult deletePersonalizedMenu(String access_token, String menuid) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/delconditional?access_token=" + access_token;
        Map<String, String> params = new HashMap<>();
        params.put("menuid", menuid);
        String result = HttpClientUtil.doPostJson(url, JSON.toJSONString(params));
        return JSON.parseObject(result, BaseResult.class);
    }

    /**
     * 测试个性化菜单匹配结果
     *
     * @param access_token 接口调用令牌
     * @param user_id      user_id可以是粉丝的OpenID，也可以是粉丝的微信号。
     * @return
     */
    public static CustomMenu tryMatchPersonalizedMenu(String access_token, String user_id) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/trymatch?access_token=" + access_token;
        Map<String, String> params = new HashMap<>();
        params.put("user_id", user_id);
        String result = HttpClientUtil.doPostJson(url, JSON.toJSONString(params));
        return JSON.parseObject(result, CustomMenu.class);
    }

}
