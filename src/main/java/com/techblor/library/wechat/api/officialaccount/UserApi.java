package com.techblor.library.wechat.api.officialaccount;

import com.alibaba.fastjson.JSON;
import com.techblor.library.wechat.beans.officialaccount.params.BatchGetUserInfoParam;
import com.techblor.library.wechat.beans.officialaccount.results.BatchGetUserInfoResult;
import com.techblor.library.wechat.beans.officialaccount.results.FollowUserListResult;
import com.techblor.library.wechat.beans.result.BaseResult;
import com.techblor.library.wechat.beans.user.BaseUserInfo;
import com.techblor.library.wechat.beans.user.Tag;
import com.techblor.library.wechat.beans.user.TagResult;
import com.techblor.library.wechat.utils.HttpClientUtil;

/**
 * 获取用户信息(未完善,未测试)
 *
 * @author zhangceven
 */
public class UserApi {

    /**
     * 获取用户的基本信息
     *
     * @param access_token
     * @param openid
     * @param lang
     * @return
     */
    public static BaseUserInfo getUserInfo(String access_token, String openid, String lang) {
        String url = "https://api.weixin.qq.com/cgi-bin/user/info"
                + "?access_token=" + access_token
                + "&openid=" + openid
                + "&lang=" + lang;

        String result = HttpClientUtil.doGet(url);
        return JSON.parseObject(result, BaseUserInfo.class);
    }

    /**
     * 批量获取用户信息
     *
     * @param access_token
     * @param param
     * @return
     */
    public static BatchGetUserInfoResult batchGetUserInfo(String access_token, BatchGetUserInfoParam param) {
        String url = "https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=" + access_token;
        String result = HttpClientUtil.doPostJson(url, JSON.toJSONString(param));
        return JSON.parseObject(result, BatchGetUserInfoResult.class);
    }

    /**
     * 创建用户标签(通过Tag对象)
     *
     * @param access_token
     * @return
     */
    public static Tag createUserTags(String access_token, Tag tag) {
        String tagJson = JSON.toJSONString(tag);
        tag = createUserTags(access_token, tagJson);
        return tag;
    }

    /**
     * 创建用户标签(通过Tag的json数据)
     *
     * @param access_token
     * @return
     */
    public static Tag createUserTags(String access_token, String tagJson) {
        String url = "https://api.weixin.qq.com/cgi-bin/tags/create"
                + "?access_token=" + access_token;

        String result = HttpClientUtil.doPostJson(url, tagJson);
        return JSON.parseObject(result, Tag.class);
    }

    /**
     * 获取公众号已创建的标签
     *
     * @param access_token
     * @return
     */
    public static TagResult getUserTags(String access_token) {
        String url = "https://api.weixin.qq.com/cgi-bin/tags/get"
                + "?access_token=" + access_token;

        String result = HttpClientUtil.doGet(url);
        return JSON.parseObject(result, TagResult.class);
    }

    /**
     * 编辑标签(通过Tag对象)
     *
     * @param access_token
     * @param tag
     * @return
     */
    public static BaseResult updateUserTags(String access_token, Tag tag) {
        String tagJson = JSON.toJSONString(tag);
        return updateUserTags(access_token, tagJson);
    }

    /**
     * 编辑标签(通过Tag的json数据)
     *
     * @param access_token
     * @param tagJson
     * @return
     */
    public static BaseResult updateUserTags(String access_token, String tagJson) {
        String url = "https://api.weixin.qq.com/cgi-bin/tags/update"
                + "?access_token=" + access_token;

        String result = HttpClientUtil.doPostJson(url, tagJson);
        return JSON.parseObject(result, BaseResult.class);

    }

    /**
     * 删除标签(通过Tag对象)
     *
     * @param access_token
     * @param tag
     * @return
     */
    public static BaseResult deleteUserTags(String access_token, Tag tag) {
        String tagJson = JSON.toJSONString(tag);
        return deleteUserTags(access_token, tagJson);

    }

    /**
     * 删除标签
     *
     * @param access_token
     * @param tagJson
     * @return
     */
    public static BaseResult deleteUserTags(String access_token, String tagJson) {
        String url = "https://api.weixin.qq.com/cgi-bin/tags/delete"
                + "?access_token=" + access_token;

        String result = HttpClientUtil.doPostJson(url, tagJson);
        return JSON.parseObject(result, BaseResult.class);
    }

    /**
     * 获取用户列表
     * 公众号可通过本接口来获取帐号的关注者列表
     * 关注者列表由一串OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。
     * 一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。
     *
     * @param access_token
     * @param next_openid
     * @return
     */
    public static FollowUserListResult getFollowUserList(String access_token, String next_openid) {
        String url = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=" + access_token
                + "&next_openid=" + next_openid;

        String result = HttpClientUtil.doGet(url);
        return JSON.parseObject(result, FollowUserListResult.class);
    }

}
