package com.techblor.library.wechat.api.miniprogram;

import com.alibaba.fastjson.JSON;
import com.techblor.library.wechat.beans.live.LiveInfo;
import com.techblor.library.wechat.utils.HttpClientUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 直播API
 */
public class LiveApi {

    /**
     * 获取直播间列表
     *
     * @param start
     * @param limit
     * @param accessToken
     * @return
     */
    public static LiveInfo getLiveInfo(int start, int limit, String accessToken) {
        String url = "https://api.weixin.qq.com/wxa/business/getliveinfo?access_token=" + accessToken;
        Map<String, Integer> params = new HashMap<>();
        params.put("start", start);
        params.put("limit", limit);
        String result = HttpClientUtil.doPostJson(url, JSON.toJSONString(params));
        return JSON.parseObject(result, LiveInfo.class);
    }
}
