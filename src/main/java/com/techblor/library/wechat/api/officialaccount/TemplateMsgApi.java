package com.techblor.library.wechat.api.officialaccount;

import com.alibaba.fastjson.JSON;
import com.techblor.library.wechat.beans.result.BaseResult;
import com.techblor.library.wechat.beans.template.TemplateMessageParams;
import com.techblor.library.wechat.utils.HttpClientUtil;

/**
 * 模板消息API
 *
 * @author zhangceven
 */
public class TemplateMsgApi {


    /**
     * 发送模板消息
     *
     * @param access_token
     * @param templateMsgJson 模板消息json数据
     * @return
     */
    public static BaseResult sendMessage(String access_token, String templateMsgJson) {
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send"
                + "?access_token=" + access_token;

        String result = HttpClientUtil.doPostJson(url, templateMsgJson);
        return JSON.parseObject(result, BaseResult.class);
    }

    /**
     * 发送模板消息
     *
     * @param access_token
     * @param templateMessageParams  模板消息数据对象
     * @return
     */
    public static BaseResult sendMessage(String access_token, TemplateMessageParams templateMessageParams) {
        String templateMsgJson = JSON.toJSONString(templateMessageParams);
        return sendMessage(access_token, templateMsgJson);

    }


}
