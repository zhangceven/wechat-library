package com.techblor.library.wechat.api.miniprogram;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.techblor.library.wechat.beans.miniprogram.params.subscribemessage.AddTemplateParams;
import com.techblor.library.wechat.beans.miniprogram.params.subscribemessage.GetPubTemplateTitleListParams;
import com.techblor.library.wechat.beans.miniprogram.params.subscribemessage.SendParams;
import com.techblor.library.wechat.beans.miniprogram.result.subscribemessage.*;
import com.techblor.library.wechat.beans.result.BaseResult;
import com.techblor.library.wechat.utils.DataConvertUtil;
import com.techblor.library.wechat.utils.HttpClientUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 订阅消息api
 */
public class SubscribeMessageApi {

    /**
     * 组合模板并添加至帐号下的个人模板库
     *
     * @param params      参数
     * @param accessToken 接口调用授权令牌
     * @return
     */
    public static AddTemplateResult addTemplate(AddTemplateParams params, String accessToken) {
        String url = "https://api.weixin.qq.com/wxaapi/newtmpl/addtemplate?access_token=" + accessToken;
        String result = HttpClientUtil.doPostJson(url, JSON.toJSONString(params));
        return JSON.parseObject(result, AddTemplateResult.class);
    }

    /**
     * 删除帐号下的个人模板
     *
     * @param priTmplId   要删除的模板id
     * @param accessToken 接口调用授权令牌
     * @return
     */
    public static BaseResult deleteTemplate(String priTmplId, String accessToken) {
        String url = "https://api.weixin.qq.com/wxaapi/newtmpl/deltemplate?access_token=" + accessToken;
        Map<String, String> params = new HashMap<>();
        params.put("priTmplId", priTmplId);
        String result = HttpClientUtil.doPostJson(url, JSON.toJSONString(params));
        return JSON.parseObject(result, BaseResult.class);
    }

    /**
     * 获取小程序账号的类目
     *
     * @param accessToken 接口调用授权令牌
     * @return
     */
    public static GetCategoryResult getCategory(String accessToken) {
        String url = "https://api.weixin.qq.com/wxaapi/newtmpl/getcategory?access_token=" + accessToken;
        String result = HttpClientUtil.doGet(url);
        return JSON.parseObject(result, GetCategoryResult.class);
    }

    /**
     * 获取模板标题下的关键词列表
     *
     * @param tid         模板标题 id: 可通过接口获取
     * @param accessToken 接口调用授权令牌
     * @return
     */
    public static GetPubTemplateKeyWordsByIdResult getPubTemplateKeyWordsById(String tid, String accessToken) {
        String url = "https://api.weixin.qq.com/wxaapi/newtmpl/getpubtemplatekeywords?access_token=" + accessToken;
        Map<String, String> params = new HashMap<>();
        params.put("tid", "tid");
        String result = HttpClientUtil.doGet(url, params);
        return JSON.parseObject(result, GetPubTemplateKeyWordsByIdResult.class);
    }

    /**
     * 获取帐号所属类目下的公共模板标题
     *
     * @param params      获取帐号所属类目下的公共模板标题参数
     * @param accessToken 接口调用授权令牌
     * @return
     */
    public static PubTemplateTitleResult getPubTemplateTitleList(GetPubTemplateTitleListParams params, String accessToken) {
        String url = "https://api.weixin.qq.com/wxaapi/newtmpl/getpubtemplatetitles?access_token=" + accessToken;
        Map<String, String> paramsMap = DataConvertUtil.objectToMap(params, new TypeReference<Map<String, String>>() {
        });
        String result = HttpClientUtil.doGet(url, paramsMap);
        return JSON.parseObject(result, PubTemplateTitleResult.class);
    }

    /**
     * 获取当前帐号下的个人模板列表
     *
     * @param accessToken 接口调用授权令牌
     * @return
     */
    public static GetTemplateListResult getTemplateList(String accessToken) {
        String url = "https://api.weixin.qq.com/wxaapi/newtmpl/gettemplate?access_token=" + accessToken;
        String result = HttpClientUtil.doGet(url);
        return JSON.parseObject(result, GetTemplateListResult.class);
    }

    /**
     * 发送订阅消息
     *
     * @param params      订阅消息发送参数
     * @param accessToken 接口调用授权令牌
     * @return
     */
    public static BaseResult send(SendParams params, String accessToken) {
        String url = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken;
        String result = HttpClientUtil.doPostJson(url, JSON.toJSONString(params));
        return JSON.parseObject(result, BaseResult.class);
    }

}
