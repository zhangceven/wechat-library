package com.techblor.library.wechat.api.officialaccount;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.techblor.library.wechat.beans.message.response.Article;
import com.techblor.library.wechat.beans.message.response.ImageMessage;
import com.techblor.library.wechat.beans.message.response.MusicMessage;
import com.techblor.library.wechat.beans.message.response.NewsMessage;
import com.techblor.library.wechat.beans.message.response.TextMessage;
import com.techblor.library.wechat.beans.message.response.VoiceMessage;
import com.thoughtworks.xstream.XStream;

/**
 * 消息api
 *
 * @author zhangceven
 */
public class MessageApi {

    public static final String MESSAGE_TEXT = "text";                // 文本
    public static final String MESSAGE_NEWS = "news";                // 图文
    public static final String MESSAGE_IMAGE = "image";                // 图片
    public static final String MESSAGE_VOICE = "voice";                // 语音
    public static final String MESSAGE_VIDEO = "video";                // 视频
    public static final String MESSAGE_LINK = "link";                // 链接
    public static final String MESSAGE_LOCATION = "localtion";        // 地理位置
    public static final String MESSAGE_EVNET = "event";                // 事件
    public static final String MESSAGE_UNSUBSCRIBE = "unsubscribe"; // 取消关注
    public static final String MESSAGE_SUBSCRIBE = "subscribe";        // 关注
    public static final String MESSAGE_CLICK = "CLICK";                // 点击
    public static final String MESSAGE_SCAN = "scan";                // 扫码

    /**
     * 将接收的消息XML转换为Map
     *
     * @param request
     * @return
     * @throws DocumentException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public static Map<String, String> xmlToMap(HttpServletRequest request) throws Exception {
        // 将解析结果存储在map中
        Map<String, String> map = new HashMap<>();
        // 从request中取得输入流
        InputStream ins = request.getInputStream();

        SAXReader reader = new SAXReader();
        Document doc = reader.read(ins);
        Element root = doc.getRootElement();
        List<Element> list = root.elements();

        for (Element e : list) {
            map.put(e.getName(), e.getText());
        }
        ins.close();
        ins = null;
        return map;
    }

    /**
     * 将响应的文本消息对象转换成XML
     *
     * @return
     */
    public static String textMessageToXml(TextMessage textMessage) {
        XStream xstream = new XStream();
        xstream.alias("xml", textMessage.getClass());
        String xml = xstream.toXML(textMessage);
        return xml;

    }

    /**
     * 将响应的图片消息对象转换成xml
     *
     * @param imageMessage
     * @return
     */
    public static String imageMesssageToXml(ImageMessage imageMessage) {
        XStream xstream = new XStream();
        xstream.alias("xml", imageMessage.getClass());
        String xml = xstream.toXML(imageMessage);
        return xml;
    }

    /**
     * 语音消息对象转换成xml
     *
     * @param voiceMessage
     * @return
     */
    public static String voiceMessageToXml(VoiceMessage voiceMessage) {
        XStream xstream = new XStream();
        xstream.alias("xml", voiceMessage.getClass());
        String xml = xstream.toXML(voiceMessage);
        return xml;
    }

    /**
     * 音乐消息对象转换成xml
     *
     * @param musicMessage
     * @return
     */
    public static String messageToXml(MusicMessage musicMessage) {
        XStream xstream = new XStream();
        xstream.alias("xml", musicMessage.getClass());
        xstream.alias("xml", musicMessage.getClass());
        String xml = xstream.toXML(musicMessage);
        return xml;
    }

    /**
     * 将图文消息转换为XML
     *
     * @param newsMessage
     * @return
     */
    public static String newsMessageToXml(NewsMessage newsMessage) {
        XStream xstream = new XStream();
        xstream.alias("xml", newsMessage.getClass());
        xstream.alias("item", new Article().getClass());
        String xml = xstream.toXML(newsMessage);
        return xml;
    }

}
