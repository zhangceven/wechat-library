package com.techblor.library.wechat.api.miniprogram;

import com.alibaba.fastjson.JSON;
import com.techblor.library.wechat.beans.miniprogram.beans.MiniProgramUserInfo;
import com.techblor.library.wechat.beans.miniprogram.beans.PhoneNumberInfo;
import com.techblor.library.wechat.beans.miniprogram.beans.RunData;
import com.techblor.library.wechat.utils.WxDecodeUtil;

/**
 * 小程序api
 *
 * @author zhangceven
 */
public class MiniProgramApi {

    /**
     * 获取手机号码信息
     *
     * @param sessionKey    code换取的sessionKey
     * @param encryptedData 包括敏感数据在内的完整用户信息的加密数据
     * @param iv            加密算法的初始向量
     * @return
     */
    public static PhoneNumberInfo getPhoneNumberInfo(String sessionKey, String encryptedData, String iv) {
        String result = WxDecodeUtil.decrypt(sessionKey, encryptedData, iv);
        return JSON.parseObject(result, PhoneNumberInfo.class);
    }

    /**
     * 获取小程序用户信息
     *
     * @param sessionKey    code换取的sessionKey
     * @param encryptedData 包括敏感数据在内的完整用户信息的加密数据
     * @param iv            加密算法的初始向量
     * @return
     */
    public static MiniProgramUserInfo getMiniProgramUserInfo(String sessionKey, String encryptedData, String iv) {
        String result = WxDecodeUtil.decrypt(sessionKey, encryptedData, iv);
        return JSON.parseObject(result, MiniProgramUserInfo.class);
    }

    /**
     * 获取运动数据
     *
     * @param sessionKey
     * @param encryptedData
     * @param iv
     * @return
     */
    public static RunData getRunData(String sessionKey, String encryptedData, String iv) {
        String result = WxDecodeUtil.decrypt(sessionKey, encryptedData, iv);
        return JSON.parseObject(result, RunData.class);
    }
}
