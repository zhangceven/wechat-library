package com.techblor.library.wechat.api.miniprogram;

import com.alibaba.fastjson.JSON;
import com.techblor.library.wechat.beans.miniprogram.params.UnlimitWxaCodeParams;
import com.techblor.library.wechat.beans.miniprogram.beans.WxaCode;
import com.techblor.library.wechat.beans.miniprogram.params.WxaCodeParams;
import com.techblor.library.wechat.utils.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;

import java.awt.image.BufferedImage;

import static com.techblor.library.wechat.constants.WechatConstant.BASE_URL;

/**
 * 微信小程序码api
 */
@Slf4j
public class WxacodeApi {

    /**
     * 获取小程序二维码,适用于需要的码数量较少的业务场景。通过该接口生成的小程序码,永久有效,有数量限制,详见获取二维码。
     * https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/qr-code.html
     *
     * @param params
     * @param accessToken
     * @return
     */
    public static WxaCode createQRCode(WxaCodeParams params, String accessToken) {
        String url = BASE_URL + "/cgi-bin/wxaapp/createwxaqrcode?access_token=" + accessToken;
        String result = HttpClientUtil.doPostJson(url, JSON.toJSONString(params));
        return JSON.parseObject(result, WxaCode.class);
    }

    /**
     * 获取小程序码,适用于需要的码数量较少的业务场景。通过该接口生成的小程序码,永久有效,有数量限制,详见获取二维码。
     * https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/qr-code.html
     *
     * @param params
     * @param accessToken
     * @return
     */
    public static WxaCode getWxacode(WxaCodeParams params, String accessToken) {
        String url = BASE_URL + "/wxa/getwxacode?access_token=" + accessToken;
        BufferedImage bufferedImage = HttpClientUtil.sendPostJson(url, JSON.toJSONString(params));
        if (null == bufferedImage) {
            return null;
        }

        WxaCode wxaCode = new WxaCode();
        wxaCode.setContentType("image/jpeg");
        wxaCode.setBuffer(bufferedImage);
        return wxaCode;
    }

    /**
     * 获取小程序码,适用于需要的码数量极多的业务场景。通过该接口生成的小程序码,永久有效,数量暂无限制。更多用法详见 获取二维码。
     * https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/qr-code.html
     *
     * @param params
     * @param accessToken
     * @return
     */
    public static WxaCode getUnlimited(UnlimitWxaCodeParams params, String accessToken) {
        String url = BASE_URL + "/wxa/getwxacodeunlimit?access_token=" + accessToken;
        BufferedImage bufferedImage = HttpClientUtil.sendPostJson(url, JSON.toJSONString(params));
        if (null == bufferedImage) {
            return null;
        }

        WxaCode wxaCode = new WxaCode();
        wxaCode.setContentType("image/jpeg");
        wxaCode.setBuffer(bufferedImage);
        return wxaCode;
    }
}
